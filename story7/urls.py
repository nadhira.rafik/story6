from django.urls import include, path
from . import views

app_name = 'story7'

urlpatterns = [
    path('', views.story7, name='story7'),
]