from django.test import TestCase, Client
from django.urls import resolve

class TestStory7(TestCase):

#urltest
    def test_url_story7(self): #GET
        response = Client().get('/story7/')
        self.assertEqual(response.status_code, 200)

#viewsTest
    def test_template_story7_is_used(self): #test template used
        response = Client().get('/story7/')
        self.assertTemplateUsed(response, 'story7.html')
    

