from django import forms

class loginForm(forms.Form):
    username = forms.CharField(widget=forms.TextInput(attrs={'class': 'form-control', 'type' : 'text', 'placeholder' : 'Username' , 'required' : True,}))
    password = forms.CharField(widget=forms.TextInput(attrs={'class' : 'form-control', 'type' : 'password', 'placeholder' : 'Password' , 'required' : True,}))

class registerForm(forms.Form):
    username = forms.CharField(widget=forms.TextInput(attrs={'class': 'form-control', 'type' : 'text', 'placeholder' : 'Username' , 'required' : True,}))
    email = forms.CharField(widget=forms.TextInput(attrs={'class': 'form-control', 'type' : 'text', 'placeholder' : 'Email' , 'required' : True,}))
    password = forms.CharField(widget=forms.TextInput(attrs={'class' : 'form-control', 'type' : 'password', 'placeholder' : 'Password' , 'required' : True,}))