from django.test import TestCase, Client
from django.urls import resolve
from django.contrib.auth.models import User

class TestStory9(TestCase):

#urltest
    def test_url_story9(self): #GET
        response = Client().get('/story9/')
        self.assertEqual(response.status_code, 200)

    def test_url_story9_login(self): #GET
        response = Client().get('/story9/login/')
        self.assertEqual(response.status_code, 200)

    def test_url_story9_register(self): #GET
        response = Client().get('/story9/register/')
        self.assertEqual(response.status_code, 200)

#viewsTest
    def test_template_story9_is_used(self): #test template used
        response = Client().get('/story9/')
        self.assertTemplateUsed(response, 'story9.html')
    
    def test_template_story9_login_is_used(self): #test template used
        response = Client().get('/story9/login/')
        self.assertTemplateUsed(response, 'login.html')
    
    def test_template_story9_register_is_used(self): #test template used
        response = Client().get('/story9/register/')
        self.assertTemplateUsed(response, 'register.html')

#userTest
    def test_story9_user(self):
        user = User.objects.create_user('admin', 'admin@gmail.com', 'admin')
        request = Client().get('/story9/')
        request.user = user
        self.assertEqual(request.status_code, 200)

