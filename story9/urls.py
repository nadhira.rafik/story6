from django.urls import include, path
from . import views

app_name = 'story9'

urlpatterns = [
    path('', views.story9, name='story9'),
     path('login/', views.login_story9, name ='login'),
     path('logout/',views.logout_story9, name='logout'),
     path('register/', views.register, name='register'),
     path('notLogin/', views.register, name='notLogin'),
]