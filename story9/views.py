from django.shortcuts import render, redirect
from .forms import loginForm, registerForm
from django.contrib.auth import authenticate, login, logout
from django.contrib.auth.models import User

def story9(request):
    return render(request, 'story9.html')

def login_story9(request):
    if request.method == "POST":
        form = loginForm(request.POST)
        if form.is_valid():
            input_username = form.cleaned_data['username']
            input_password = form.cleaned_data['password']
            user = authenticate(request, username = input_username, password = input_password)
            if user is not None:
                login(request, user)
                return redirect('/story9/')
            else:
                return redirect('/story9/register/')
    else:
        form = loginForm()
        response = {'form' : form}
        return render(request, "login.html", response)

def logout_story9(request):
    logout(request)
    return redirect('/story9/')

def register(request):
    if request.method == "POST":
        form = registerForm(request.POST)
        if form.is_valid():
            input_username = form.cleaned_data["username"]
            input_email = form.cleaned_data["email"]
            input_password = form.cleaned_data["password"]
            try:
                user = User.objects.get(username = input_username)
                return redirect("/story9/register/")
            except User.DoesNotExist:
                user = User.objects.create_user(input_username, input_email, input_password)
                return redirect("/story9/login/")
    else:
        form = registerForm()
        response = {'form' : form}
        return render(request, "register.html", response)