from django.shortcuts import render
from django.http import HttpResponseRedirect, JsonResponse

def story8(request):
    return render(request, 'story8.html')

def caribuku(request):
    arg = request.GET['q']
    url_data = 'https://www.googleapis.com/books/v1/volumes?q=' + arg
    result = requests.get(url_data)
    data = json.loads(result.content)
    return JsonResponse(data, safe=False)