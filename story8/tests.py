from django.test import TestCase, Client
from django.urls import resolve

class TestStory8(TestCase):

#urltest
    def test_url_story8(self): #GET
        response = Client().get('/story8/')
        self.assertEqual(response.status_code, 200)

#viewsTest
    def test_template_story8_is_used(self): #test template used
        response = Client().get('/story8/')
        self.assertTemplateUsed(response, 'story8.html')
   
