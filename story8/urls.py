from django.urls import include, path
from . import views

app_name = 'story8'

urlpatterns = [
    path('', views.story8, name='story8'),
    path('data_buku/', views.caribuku),
]